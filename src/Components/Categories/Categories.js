import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Table, Space, Row, Col, Input, Button, Modal, Form } from 'antd';
import { AppstoreAddOutlined } from '@ant-design/icons';
import axios from 'axios';

Categories.propTypes = {};

function Categories(props) {
    const { Search } = Input;
    const [visible, setVisible] = useState(false);
    const [edit, setEdit] = useState(false);
    const [remove, setRemove] = useState(false);
    const [fetch, setFetch] = useState(false);
    const [form] = Form.useForm();
    const [data, setData] = useState([]);
    const [idCategory, setIdCategory] = useState("");
    const [addCategories, setAddCategories] = useState({ categoryName: "" });

    const columns = [
        {
            title: 'STT',
            dataIndex: 'sttCategory',
            key: 'sttCategory',
        },
        {
            title: 'Tên Loại Sản Phẩm',
            dataIndex: 'nameCategory',
            key: 'nameCategory',
        },
        {
            title: 'Hoạt Động',
            key: 'action',
            render: (text, record) => (
                <Space size="middle">
                    <a>Invite {record.name}</a>
                    <Button
                        type="primary"
                        onClick={() => {
                            setRemove(true);
                            getValue(record);
                        }}
                        danger
                    >
                        Xóa
                    </Button>
                </Space>
            ),
        },
    ];

    const layout = {
        labelCol: { span: 8 },
        wrapperCol: { span: 16 },
    };

    const validateMessages = {
        required: '${label} không được để trống!',
    };

    const getValue = (record) => {
        console.log('record', record);
        form.setFieldsValue({
            categoryNameUpdate: record.nameCategory,
        });
        setIdCategory(record.id);
    };

    useEffect(async () => {
        const result = await axios("https://order-fastfood.herokuapp.com/api/category/");
        const { data } = result.data;

        let array = [];
        data.map((i, index) => {
            array.push({ sttCategory: index + 1, id: i._id, nameCategory: i.categoryName });
        });
        console.log('array', array);
        setData(array);
        // getCategory();
    }, [fetch]);

    const handleAddCategory = (value) => {
        console.log('add category');
        console.log('addCate', value);
        console.log('addCategories', addCategories);
        const requestOptions = {
            url: 'https://order-fastfood.herokuapp.com/api/category/',
            method: 'post',
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json",
            },
            data: JSON.stringify(addCategories)
        }
        axios(requestOptions).then(response => {
            console.log('requestOptions', requestOptions);
            console.log('response', response);
            setVisible(false);
            setFetch(!fetch);
        })
    }

    const onChange = (e) => {
        const { name, value } = e.target;
        console.log(`${name}: ${value}`);
        setAddCategories({ ...addCategories, [name]: value });
    };

    const handleEditCategory = () => {
        console.log('Edit category');
    }

    const handleRemoveCategory = (value) => {
        axios
            .delete("https://order-fastfood.herokuapp.com/api/category/" + idCategory)
            .then(function (data) {
                setFetch(!fetch);
                setRemove(false);
            });
    }

    return (
        <div className="section-categories">
            <div className="container-fluid">
                <div className="section-product__title">
                    <h1>Quản Lý Loại Sản Phẩm</h1>
                </div>

                <div className="section-product__func">
                    <Row>
                        <Col span={8}>
                            <div className="section-product__func--search">
                                <Search placeholder="input search text" enterButton />
                            </div>
                        </Col>
                        <Col span={16}>
                            <div className="section-product__func--btn d-flex justify-content-end">
                                <Button type="primary" onClick={() => setVisible(true)}>
                                    <AppstoreAddOutlined style={{ fontSize: '2rem' }} />
                                    Thêm Loại Sản Phẩm
                                </Button>
                                <Modal
                                    title="Thêm loại sản phẩm"
                                    onCancel={() => setVisible(false)}
                                    visible={visible}
                                    cancelText="Thoát"
                                    footer={[
                                        <Button form="addCategory" type="primary" htmlType="submit">
                                            Submit
                                        </Button>
                                    ]}
                                >
                                    <Form
                                        id="addCategory"
                                        name="nest-messages"
                                        validateMessages={validateMessages}
                                        onFinish={handleAddCategory}
                                    >
                                        <Form.Item name="categoryName" label="Tên sản phẩm" rules={[{ required: true }]}>
                                            <Input name="categoryName" onChange={(e) => onChange(e)} />
                                        </Form.Item>
                                    </Form>
                                </Modal>

                                <Modal
                                    form={form}
                                    id="updateCategory"
                                    title="Chỉnh sửa loại sản phẩm"
                                    onCancel={() => setEdit(false)}
                                    visible={edit}
                                    cancelText="Thoát"
                                    footer={[
                                        <Button
                                            onClick={() => handleEditCategory()}
                                            type="primary"
                                            htmlType="submit"
                                        >
                                            Xóa
                                        </Button>
                                    ]}
                                >
                                    <Form
                                        id="editCategory"
                                        name="nest-messages"
                                        validateMessages={validateMessages}
                                        onFinish={handleEditCategory}
                                    >
                                        <Form.Item name="categoryNameUpdate" label="Tên sản phẩm" rules={[{ required: true }]}>
                                            <Input name="categoryNameUpdate" onChange={(e) => onChange(e)} />
                                        </Form.Item>
                                    </Form>
                                </Modal>

                                <Modal
                                    title="Xóa loại sản phẩm"
                                    onCancel={() => setRemove(false)}
                                    visible={remove}
                                    cancelText="Thoát"
                                    footer={[
                                        <Button
                                            onClick={() => handleRemoveCategory()}
                                            type="primary"
                                            htmlType="submit"
                                        >
                                            Xóa
                                        </Button>
                                    ]}
                                >
                                    <h3>Bạn có chắc muốn xóa loại sản phẩm!</h3>
                                </Modal>
                            </div>
                        </Col>
                    </Row>
                </div>

                <div className="section-product__content">
                    <Table columns={columns} dataSource={data} />
                </div>
            </div>
        </div>
    );
}

export default Categories;