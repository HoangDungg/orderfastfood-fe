import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { useRecoilState } from "recoil";
import { userData } from "../../state/atom";
import { useCookies } from 'react-cookie';
import { useHistory } from 'react-router-dom';
import { Form, Input, Button, Checkbox, Space, notification } from "antd";
import {
  LoadingOutlined,
  CheckCircleOutlined,
  WarningOutlined,
  UserOutlined,
  LockOutlined,
} from "@ant-design/icons";
import "antd/dist/antd.css";


export default function FormLogin(props) {
  const [user, setUser] = useRecoilState(userData);

  const [cookies, setCookie] = useCookies(['cookieUser']);

  const [data, setData] = useState([]);

  let history = useHistory();

  const onFinish = (values) => {
    console.log('Success:', values);
    let data = {
      email: values.email,
      password: values.password
    }
    console.log('data', data);
    axios.post(
      'https://order-fastfood.herokuapp.com/api/user/login', data
    ).then(function (response) {
      console.log(response);
      const dataUser = response.data.email;
      setCookie('cookieUser', response.data.token, { path: '/' });
      setUser(dataUser);
      history.push("/home");
    }).catch(function (error) {
      notification.open({
        message: error.response.data.msg
      });
    });
  };

  return (
    <div className="mt-5 d-flex justify-content-center section-form__content--form">
      <Form
        name="login"
        className="m-auto login-form"
        // initialValues={{ remember: true }}
        onFinish={onFinish}
      >
        <Form.Item
          name="email"
          rules={[{ required: true, message: "Email không được bỏ trống!" }]}
        >
          <Input
            prefix={<UserOutlined className="site-form-item-icon" />}
            placeholder="Email"
          />
        </Form.Item>
        <Form.Item
          name="password"
          rules={[{ required: true, message: "Password không được bỏ trống!" }]}
        >
          <Input
            prefix={<LockOutlined className="site-form-item-icon" />}
            type="password"
            placeholder="Password"
          />
        </Form.Item>
        <Form.Item>
          <Form.Item name="remember" valuePropName="checked" noStyle>
            <Checkbox>Remember me</Checkbox>
          </Form.Item>

          <a className="login-form-forgot" href="">
            Forgot password
          </a>
        </Form.Item>

        <Form.Item>
          <Space size="middle">
            <Button
              type="primary"
              htmlType="submit"
              className="login-form-button"
            >
              Sign in
              {/* {isLoading ? <LoadingOutlined /> : "Sign in"} */}
            </Button>

          </Space>
        </Form.Item>
      </Form>
    </div>
  );
}
