import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import { Menu, Button } from 'antd';
import { AppstoreOutlined, DatabaseOutlined, MenuUnfoldOutlined, MenuFoldOutlined, PieChartOutlined, DesktopOutlined, ContainerOutlined, MailOutlined, } from '@ant-design/icons';
import Logo from '../../assets/images/logo/fast-food.jpg';

index.propTypes = {};
const { SubMenu } = Menu;
function index(props) {

    return (
        <div className="section-menu">
            <div className="section-menu__logo d-flex justify-content-center">
                <a href="/home">
                    <img src={Logo} className="img-fluid" />
                </a>
            </div>

            <div className="section-menu__list-menu h-100">
                <Menu
                    defaultSelectedKeys={['1']}
                    defaultOpenKeys={['sub1']}
                    mode="inline"
                    theme="dark"
                    className="h-100"
                >
                    <Menu.Item key="1" >
                        <NavLink to="/home" >
                            <AppstoreOutlined style={{ fontSize: '2rem' }} />
                            Quản lý sản phẩm
                        </NavLink>
                    </Menu.Item>
                    <Menu.Item key="2">
                        <NavLink to="/home/categories">
                            <DatabaseOutlined style={{ fontSize: '2rem' }} />
                            Quản lý loại sản phẩm
                        </NavLink>
                    </Menu.Item>
                    <Menu.Item key="3" icon={<ContainerOutlined />}>
                        Option 3
                    </Menu.Item>
                </Menu>
            </div>
        </div>
    );
}

export default index;