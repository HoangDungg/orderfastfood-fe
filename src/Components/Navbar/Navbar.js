import React from 'react';
import PropTypes from 'prop-types';
import { Menu, Space } from 'antd';
import { MailOutlined, BellOutlined, AppstoreOutlined, SettingOutlined } from '@ant-design/icons';
import { useRecoilValue } from 'recoil';
import Avata from '../../assets/images/form_customer/login_img.jpg';
import { userData } from '../../state/atom';
Navbar.propTypes = {};

const { SubMenu } = Menu;

function Navbar(props) {
    const userName = useRecoilValue(userData);

    return (
        <div className="section-navbar">
            <div className="container-fluid">
                <Menu className="d-flex justify-content-end" mode="horizontal">
                    <Space>
                        <Menu.Item className="section-navbar__item section-navbar__mail" key="mail">
                            <MailOutlined className="d-flex align-items-center justify-content-center h-100" />
                        </Menu.Item>
                        <Menu.Item className="section-navbar__item section-navbar__notifi" key="notification">
                            <BellOutlined className="d-flex align-items-center justify-content-center h-100" />
                        </Menu.Item>
                        <Menu.Item className="section-navbar__item" key="">
                            <div className="user d-flex">
                                <div className="user-avt d-flex align-items-center">
                                    <img className="img-fluid" src={Avata} />
                                </div>
                                <div className="user-desc d-flex align-items-center h-100">
                                    <div className="user-desc__content">
                                        <h3 className="d-flex align-items-center">{userName}</h3>
                                    </div>
                                </div>
                            </div>
                        </Menu.Item>
                    </Space>
                </Menu>
            </div>

        </div>
    );
}

export default Navbar;