import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import {
    Table,
    Space,
    Row,
    Col,
    Button,
    Form,
    Input,
    InputNumber,
    Modal,
} from "antd";
import { AppstoreAddOutlined } from "@ant-design/icons";
import axios from "axios";

Product.propTypes = {};

const { Search } = Input;

function Product(props) {
    const [visible, setVisible] = useState(false);
    const [update, setUpdate] = useState(false);
    const [remove, setRemove] = useState(false);
    const [form] = Form.useForm();
    const [idProduct, setIdProduct] = useState("");
    const [fetch, setFetch] = useState(false);
    const [categories, setCategories] = useState([]);
    const [data, setData] = useState([]);
    const columns = [
        {
            title: "STT",
            dataIndex: "sttProduct",
            key: "sttProduct",
            render: (text) => <a>{text}</a>,
        },
        {
            title: "Tên Sản Phẩm",
            dataIndex: "nameProduct",
            key: "nameProduct",
        },
        {
            title: "Loại Sản Phẩm",
            dataIndex: "categoryProduct",
            key: "categoryProduct",
        },
        {
            title: "Giá Sản Phẩm",
            dataIndex: "priceProduct",
            key: "priceProduct",
        },
        {
            title: "Mô Tả Sản Phẩm",
            dataIndex: "decsProduct",
            key: "decsProduct",
        },
        {
            title: "Hoạt Động",
            key: "action",
            render: (text, record) => (
                <Space size="middle">
                    <Button
                        type="primary"
                        onClick={() => {
                            setUpdate(true);
                            getValue(record);
                        }}
                    >
                        Chỉnh sửa
                    </Button>
                    <Button
                        type="primary"
                        onClick={() => {
                            setRemove(true);
                            getValue(record);
                        }}
                        danger
                    >
                        Xóa
                    </Button>
                </Space>
            ),
        },
    ];
    const [addFood, setAddFoodData] = useState({
        foodName: "",
        price: "",
        desc: "",
        categories: "",
    });

    useEffect(async () => {
        const result = await axios("https://order-fastfood.herokuapp.com/api/food");
        const { data } = result.data;

        let array = [];
        data.map((i, index) => {
            array.push({
                sttProduct: index + 1,
                id: i._id,
                nameProduct: i.foodName,
                priceProduct: i.price,
                categoryProduct: i.categories.categoryName,
                decsProduct: i.desc,
            });
        });
        console.log('array', array);
        setData(array);
        getCategory();
    }, [fetch]);

    const validateMessages = {
        required: "${label} không được để trống!",
        types: {
            number: "${label} is not a valid number!",
        },
        number: {
            range: "${label} giá sản phẩm tối thiểu phải là ${min} và tối đa ${max}",
        },
    };

    const layout = {
        labelCol: { span: 8 },
        wrapperCol: { span: 16 },
    };

    const getValue = (record) => {
        console.log('record', record);
        form.setFieldsValue({
            productNameUpdate: record.nameProduct,
            productCategoryUpdate: record.categoryProduct,
            productPriceUpdate: record.priceProduct,
            productDescUpdate: record.decsProduct,
        });
        setIdProduct(record.id);
    };

    const onChange = (e) => {
        const { name, value } = e.target;
        console.log(`${name}: ${value}`);
        setAddFoodData({ ...addFood, [name]: value });
    };

    const getCategory = () => {
        const requestOption = {
            url: `https://order-fastfood.herokuapp.com/api/category`,
            method: "get",
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json",
            },
        };

        axios(requestOption).then((response) => {
            console.log(response.data.data);
            setCategories(response.data.data);
        });
    };

    const onFinish = (value) => {
        console.log("value", addFood);
        const requestOptions = {
            url: 'https://order-fastfood.herokuapp.com/api/food',
            method: 'post',
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json",
            },
            data: JSON.stringify(addFood)
        }
        axios(requestOptions).then(response => {
            console.log(response);
            setVisible(false);
            setFetch(!fetch);
        })
    };

    const onUpdate = (value) => {
        console.log("value", value);
        const data = {
            categories: [{ name: value.productCategoryUpdate }],
            foodName: value.productNameUpdate,
            price: value.productPriceUpdate,
            desc: value.productDescUpdate,
        };
        axios
            .put("https://order-fastfood.herokuapp.com/api/food/" + idProduct, data)
            .then(function (data) {
                console.log('update success!!!')
                setUpdate(false);
                setFetch(!fetch);
                console.log("response", data);
            });
    };

    const onRemove = (value) => {
        console.log(value);
        axios
            .delete("https://order-fastfood.herokuapp.com/api/food/" + idProduct)
            .then(function (data) {
                setFetch(!fetch);
                console.log("response", value);
                setRemove(false);
            });
    }

    return (
        <div className="section-product">
            <div className="container-fluid">
                <div className="section-product__title">
                    <h1>Quản Lý Sản Phẩm</h1>
                </div>

                <div className="section-product__func">
                    <Row>
                        <Col span={8}>
                            <div className="section-product__func--search">
                                <Search placeholder="input search text" enterButton />
                            </div>
                        </Col>
                        <Col span={16}>
                            <div className="section-product__func--btn d-flex justify-content-end">
                                <Button type="primary" onClick={() => setVisible(true)}>
                                    <AppstoreAddOutlined style={{ fontSize: "2rem" }} />
                                    Thêm Sản Phẩm
                                </Button>
                                <Modal
                                    title="Thêm sản phẩm"
                                    visible={visible}
                                    onCancel={() => setVisible(false)}
                                    cancelText="Thoát"
                                    footer={[
                                        <Button form="addProduct" type="primary" htmlType="submit">
                                            Submit
                                        </Button>
                                    ]}
                                >
                                    <Form
                                        id="addProduct"
                                        {...layout}
                                        name="nest-messages"
                                        validateMessages={validateMessages}
                                        onFinish={onFinish}
                                    >
                                        <Form.Item
                                            name="foodName"
                                            label="Tên sản phẩm"
                                            rules={[{ required: true }]}
                                        >
                                            <Input name="foodName" onChange={(e) => onChange(e)} />
                                        </Form.Item>
                                        <Form.Item
                                            name="categories"
                                            label="Loại sản phẩm"
                                            rules={[{ required: true }]}
                                        >
                                            <Input name="categories" onChange={(e) => onChange(e)} />
                                        </Form.Item>
                                        <Form.Item
                                            name="price"
                                            label="Giá sản phẩm"
                                        >
                                            <Input name="price" onChange={(e) => onChange(e)} />
                                        </Form.Item>
                                        <Form.Item name="desc" label="Mô tả sản phẩm">
                                            <Input name="desc" onChange={(e) => onChange(e)} />
                                        </Form.Item>
                                    </Form>
                                </Modal>

                                <Modal
                                    title="Chỉnh sửa sản phẩm"
                                    visible={update}
                                    onCancel={() => setUpdate(false)}
                                    cancelText="Thoát"
                                    footer={[
                                        <Button
                                            form="updateProduct"
                                            type="primary"
                                            htmlType="submit"
                                        >
                                            Chỉnh sửa
                                        </Button>
                                    ]}
                                >
                                    <Form
                                        form={form}
                                        id="updateProduct"
                                        {...layout}
                                        name="nest-messages"
                                        onFinish={onUpdate}
                                    >
                                        <Form.Item
                                            name="productNameUpdate"
                                            label="Tên sản phẩm"
                                        >
                                            <Input />
                                        </Form.Item>
                                        <Form.Item
                                            name="productCategoryUpdate"
                                            label="Loại sản phẩm"
                                        >
                                            <Input />
                                        </Form.Item>
                                        <Form.Item
                                            name="productPriceUpdate"
                                            label="Giá sản phẩm"
                                            rules={[{ type: "number", min: 1, max: 30 }]}
                                        >
                                            <InputNumber />
                                        </Form.Item>
                                        <Form.Item name="productDescUpdate" label="Mô tả sản phẩm">
                                            <Input />
                                        </Form.Item>
                                    </Form>
                                </Modal>

                                <Modal
                                    title="Xóa sản phẩm"
                                    visible={remove}
                                    onCancel={() => setRemove(false)}
                                    cancelText="Thoát"
                                    footer={[
                                        <Button
                                            onClick={() => onRemove()}
                                            type="primary"
                                            htmlType="submit"
                                        >
                                            Xóa
                                        </Button>
                                    ]}
                                >
                                    <h3>Bạn có chắc muốn xóa sản phẩm!</h3>
                                </Modal>

                                <Modal
                                    title="Xóa sản phẩm"
                                    visible={remove}
                                    onCancel={() => setRemove(false)}
                                    cancelText="Thoát"
                                    footer={[
                                        <Button
                                            onClick={() => onRemove()}
                                            type="primary"
                                            htmlType="submit"
                                        >
                                            Xóa
                                        </Button>
                                    ]}
                                >
                                    <h3>Bạn có chắc muốn xóa sản phẩm!</h3>
                                </Modal>
                            </div>
                        </Col>
                    </Row>
                </div>

                <div className="section-product__content">
                    <Table columns={columns} dataSource={data} />
                </div>
            </div>
        </div>
    );
}

export default Product;
