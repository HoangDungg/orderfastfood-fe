import { Switch, Route, BrowserRouter as Router, Link, Redirect } from "react-router-dom";
import { useCookies } from 'react-cookie';
import "./App.css";
import "../src/assets/scss/styles.scss";


//Pages import
import Login from "./pages/Login";
import Home from "./pages/Home";
// import Home from "./pages/";
import NotFound404 from "./pages/Notfound404";

function App() {
  const [cookies, setCookie, removeCookie] = useCookies(['cookieUser']);

  return (
    <Router>
      <Switch>
        <Route path="/" exact>
          <Login />
        </Route>
        <Route
          render={() => Object.keys(cookies).length === 0 ? (<Redirect
            to={{
              pathname: "/",
            }}
          />) : (<Home />)}
          path="/home"
        />
        <Route path="*">
          <NotFound404 />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
