import { atom } from 'recoil';

const userData = atom({
    key: 'userState',
    default: '',
});

export { userData };

