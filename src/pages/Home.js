import React from 'react';
import PropTypes from 'prop-types';
import { Switch, Route, BrowserRouter as Router, Link } from "react-router-dom";
import { withCookies, Cookies, useCookies } from 'react-cookie';
import { Row, Col, Button } from 'antd';

import Menu from '../Components/Menu';
import Navbar from '../Components/Navbar/Navbar';
import Product from '../Components/Product/Product';
import Categories from '../Components/Categories/Categories';


Home.propTypes = {};

function Home(props) {

    return (
        <div className="wrapper">
            <Row>
                <Col span={4}>
                    <Menu />
                </Col>

                <Col span={20}>
                    <Navbar />

                    <Switch>
                        <Route path="/home" exact>
                            <Product />
                        </Route>
                        <Route path="/home/categories">
                            <Categories />
                        </Route>
                        <Route path="*">
                            <h2>not found</h2>
                        </Route>

                    </Switch>

                </Col>

            </Row>
        </div>
    );
}

export default withCookies(Home);