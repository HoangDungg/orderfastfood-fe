import React from "react";

export default function Notfound404() {
  return (
    <div className="middle-font">
      <h1>Page not found 😥</h1>
    </div>
  );
}
